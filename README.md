-Comando para habilitar o terminal a executar scripts
Set-ExecutionPolicy RemoteSigned

-Executar o Json Server
json-server --watch db.json --port 3004

-Executar o projeto
npm start