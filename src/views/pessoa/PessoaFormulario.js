import React, { useState, useEffect } from 'react';
import { useHistory, useParams} from "react-router-dom";
import { connect } from "react-redux";

import {salvarPessoa, buscarPessoa, setPessoaItem} from "../../redux/actions/PessoaAction";
import {pessoaFilter} from "../../redux/filters/PessoaFilter";

import ViaCepService from "../../service/ViaCepService";

import { Formik, Form, Field, FieldArray, ErrorMessage} from 'formik';
import * as Yup from 'yup';

function PessoaFormulario(props){
    
    /* HOOKS */
    let history = useHistory();
    let {id} = useParams();
    useEffect(() => {
        buscarPessoa();
    }, []);

    /* STATES DO COMPONENTE*/


    /* MÉTODOS REDUX */
    const buscarPessoa = () => {
        if(id != undefined){
            props.buscarPessoa(id);
        } else {
            props.setPessoaItem({});
        }
        
    }

    /* MÉTODOS DA VIEW */
    const handleSubmit = (values, {setSubmitting} ) => {
        props.salvarPessoa(values);
        setSubmitting(false);
        history.push("/pessoa/lista");
    }

    const mapStateToObject = () => {
        return {
            id: props.pessoaItem.id || undefined,
            nome: props.pessoaItem.nome || '',
            cpf: props.pessoaItem.cpf || '',
            dtNascimento: props.pessoaItem.dtNascimento || '',
            enderecos: props.pessoaItem.enderecos || [ enderecosValues ]
        }
    }

    const enderecosValues = {
        id: undefined,
        logradouro: "",
        complemento: "",
        cep: ""
    }

    const validationSchema = Yup.object().shape({
        nome: Yup
            .string()
                .required("Nome deve ser um texto.") // i18n
                .min(3, "Nome deve ter no mínimo 3 caracteres.")
                .max(100, "Nome deve ter no máximo 100 caracteres."),
        cpf: Yup
            .string()
                .required(),
        dtNascimento: Yup
            .date()
                .required(),
        enderecos: Yup.array()
            .of(
                Yup.object().shape({
                    logradouro: Yup.string().required('Campo obrigatório.'),
                    cep: Yup.string().required('Campo obrigatório.')
                })
            )



    });

    
    return(
        <div>
            <h1>Cadastro de Pessoa</h1>

            <Formik 
                initialValues={ mapStateToObject() }
                validationSchema={validationSchema}
                enableReinitialize
                onSubmit={ (values, {setSubmitting} ) => handleSubmit(values, {setSubmitting} ) }

            >

                {  ({ values, handleSubmit, handleChange, isSubmitting, setFieldValue, setValues }) => (

                    <Form onSubmit={handleSubmit}>

                        <div className="mb-3">
                            <label name="nome" className="form-label">Nome:</label>
                            <Field type="text" name="nome" className="form-control" />
                            <ErrorMessage className="error-message" name="nome" component="div" />
                        </div>

                        <div className="mb-3">
                            <label className="form-label">CPF:</label>
                            <Field type="text" name="cpf" className="form-control" />
                            <ErrorMessage className="error-message" name="cpf" component="div" />
                        </div>

                        <div className="mb-3">
                            <label className="form-label">Data de nascimento:</label>
                            <Field type="date" name="dtNascimento" className="form-control" />
                            <ErrorMessage className="error-message" name="dtNascimento" component="div" />
                        </div>

                        <hr/>
                        <h3>Endereços</h3>
                        <FieldArray
                            name="enderecos"
                            render={ ({ insert, remove, push }) => (

                                <div>

                                    {values.enderecos.length > 0 && values.enderecos.map( (endereco, index) => (
    
                                        <div key={index} className="row g-3 m-0 p-3 align-items-end border-box">
    

                                                <div className="col-md-12">
                                                    <label className="form-label">Logradouro:</label>
                                                    <Field type="text" name={`enderecos.${index}.logradouro`} className="form-control" />
                                                    <ErrorMessage className="error-message" name={`enderecos.${index}.logradouro`} component="div" />
                                                </div>
                                           
    
                                                <div className="col-md-6">
                                                    <label className="form-label">Complemento:</label>
                                                    <Field type="text" name={`enderecos.${index}.complemento`}  className="form-control" />
                                                    <ErrorMessage className="error-message" name={`enderecos.${index}.complemento`} component="div" />
                                                </div>
    
                                                <div className="col-md-6">
                                                    <label className="form-label">Cep:</label>
                                                    <Field 
                                                        type="text" 
                                                        name={`enderecos.${index}.cep`} 
                                                        className="form-control" 
                                                        onChange={ (e, selected) => {
                                                            console.log(e);
                                                            const cep = e.target.value;
                                                            setFieldValue(`enderecos.${index}.cep`, cep);

                                                            if(cep.length == 8){
                                                                ViaCepService.findCep(cep)
                                                                    .then(response => {
                                                                        const logradouro = response.data.logradouro + ". "+ response.data.bairro + ". "+ response.data.localidade + " - "+response.data.uf;
                                                                        setFieldValue(`enderecos.${index}.logradouro`, logradouro)
                                                                        setFieldValue(`enderecos.${index}.complemento`, response.data.complemento)
                                                                    })
                                                            }


                                                            <div></div>

                                                        }}
                                                    />
                                                    <ErrorMessage className="error-message" name={`enderecos.${index}.cep`} component="div" />
                                                </div>
    
                                            
                                        
                                            {values.enderecos.length > 1 && 
                                                <div className="col-md-1 align-bottom">
                                                    <button type="button" className="btn btn-danger" onClick={() => remove(index)} > X </button>
                                                </div>
                                            }

                                        </div>
                                        
                                     ))}

                                    <div className="row g-3 m-0 p-0 mt-3">
                                        <div className="col-md-5">
                                            <button type="button" className="btn btn-primary " onClick= { () => push(enderecosValues) } >Adicionar Endereço</button>
                                        </div>
                                    </div>
                                     
                                </div>

                            )}
                        />


                        <div className="row g-3 m-0 p-0 mt-3">  
                            <div className="col-md-5">
                                <button type="submit" className="btn btn-primary" disabled={isSubmitting} >Salvar</button>
                            </div>
                        </div>

                        <br/>
                        <br/>
                        {JSON.stringify(values)}
                        <br/>
                        <br/>

                    </Form>


                )}



            </Formik>


        </div>
    );
}

export default connect(pessoaFilter, {salvarPessoa, buscarPessoa,setPessoaItem})(PessoaFormulario);
