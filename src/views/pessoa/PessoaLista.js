import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";

import {listarPessoa, excluirPessoa, setPessoaItem} from "../../redux/actions/PessoaAction";
import {pessoaFilter} from "../../redux/filters/PessoaFilter"

import {
    BrowserRouter as Router,
    Link
  } from "react-router-dom";

  import '../../Styles.css';


  import Modal from 'react-bootstrap/Modal';
  import Button from 'react-bootstrap/Button'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faTrashAlt, faSearch  } from '@fortawesome/free-solid-svg-icons';


function PessoaLista(props){

    /* HOOKS */
    useEffect(() => {
        listar();
    }, []);

    /* STATES */
    const [showState, setShowState] = useState(false);
    const [searchState, setSearchState] = useState('');

    /* MÉTODOS REDUX */
    const listar = () => {
        props.listarPessoa();
    }


    /* MÉTODOS DA VIEW 
        handle -> cuidam da ação específica de um componente da view
    */
    const handleShow = (pessoa) => { 
        props.setPessoaItem(pessoa);
        setShowState(true); 
    }
    const handleClose = () => setShowState(false);
    const handleSearch = (event) => setSearchState(event.target.value);
    const handleExcluir = () => { 
        props.excluirPessoa(props.pessoaItem.id);
        setShowState(false);
    };

    const filterSearch = (pessoa) => {
        return (
            pessoa.nome.toLowerCase().includes(searchState.toLowerCase()) || 
            pessoa.cpf.toLowerCase().includes(searchState.toLowerCase()) 
        )
    }

    return(
        <div>
            <h1>Lista de Pessoa</h1>

            <div className="container mtb-10" >
                <div className="row">
                    <div className="col-sm text-align-left mrl-n10">
                        <Link to={`/pessoa/formulario/`} className="btn btn-primary">Novo</Link>
                    </div>
                    <div className="col-sm text-align-right mrl-n10">
                        <div className="input-group flex-nowrap">
                            <input type="text" className="form-control" placeholder="Username" aria-label="Username" aria-describedby="addon-wrapping" onChange={handleSearch} />
                            <span className="input-group-text" id="addon-wrapping"><FontAwesomeIcon  icon={faSearch} /></span>
                        </div>
                    </div>
                </div>
            </div>

            
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">Detalhes</th>
                        <th scope="col">#</th>
                        <th scope="col">Nome</th>
                        <th scope="col">CPF</th>
                        <th scope="col">Operações</th>
                    </tr>
                </thead>
                <tbody>

                    {props.pessoaLista
                    .filter(filterSearch)
                    .map( (pessoa, index) => {
                        return (
                            <tr key={index}>
                                <th scope="row">
                                <Link to={`/pessoa/detalhes/${pessoa.id}`} className="btn btn-outline-dark mrl-10"><FontAwesomeIcon icon={faSearch} /></Link>
                                </th>
                                <th scope="row">{pessoa.id}</th>
                                <td>{pessoa.nome}</td>
                                <td>{pessoa.cpf}</td>
                                <td>
                                    <Link to={`/pessoa/formulario/${pessoa.id}`} className="btn btn-outline-dark mrl-10"><FontAwesomeIcon icon={faPencilAlt} /></Link>
                                    <Button  variant="outline-dark" onClick={ () => handleShow(pessoa)  } ><FontAwesomeIcon icon={faTrashAlt} /></Button>
                                    
                                </td>
                            </tr>
                        );
                        

                    })}

                </tbody>
            </table>

            


            <Modal show={showState} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Confirmar exclusão</Modal.Title>
                </Modal.Header>
                <Modal.Body>Deja realmente exluir o, {props.pessoaItem.nome}</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cancelar
                    </Button>
                    <Button variant="danger" onClick={handleExcluir}>
                        Excluir
                    </Button>
                </Modal.Footer>
            </Modal>
            

        </div>
    );
}

export default connect(pessoaFilter,{listarPessoa, excluirPessoa, setPessoaItem})(PessoaLista);
