//import React from 'react';
import axios from 'axios'; 

const API_URL = "http://localhost:8080/pessoa/";


const listar = () => {
    return axios.get( API_URL);

}

const buscarPeloId = (id) => {
    return axios.get( API_URL+id);
}

const salvar = (pessoa) => {

    if(pessoa.id == undefined){
        return axios.post(API_URL,pessoa)
    } 
    else {
        return axios.put(API_URL,pessoa)
    }

}

const excluir = (id) => {
    return axios.delete( API_URL+id);
}


export default {
    listar,
    buscarPeloId,
    salvar,
    excluir
}
