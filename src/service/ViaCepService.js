import axios from 'axios';


const API_URL = "https://viacep.com.br/ws/";

const findCep = (cep) => {
    return axios.get( API_URL+"/"+cep+"/json/");
}



export default { findCep }