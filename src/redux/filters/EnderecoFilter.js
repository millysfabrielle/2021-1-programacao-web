import values from "lodash/values";

export const enderecoFilter = ( {enderecoState} ) => {
    return { 
        enderecoItem: enderecoState.enderecoItem,
        enderecoLista: values(enderecoState.enderecoLista)
    }
}