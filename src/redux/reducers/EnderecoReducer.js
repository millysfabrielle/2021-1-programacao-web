import {ENDERECO_ACTIONS} from "../actions/EnderecoAction";

const enderecoState = {
    enderecoLista: [],
    enderecoItem: {}
}

export default function enderecoReducer(state = enderecoState, callback){

    switch(callback.type){

        case ENDERECO_ACTIONS.LISTAR:
            return {
                ...state,
                enderecoLista: callback.content
            }


        default:
            return state;
    }

}