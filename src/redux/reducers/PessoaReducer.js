import {PESSOA_ACTIONS} from "../actions/PessoaAction";

const pessoaState = {
    pessoaLista: [],
    pessoaItem: {}
}

export default function pessoaReducer(state = pessoaState, callback){

    switch(callback.type){

        case PESSOA_ACTIONS.LISTAR:
            return {
                ...state,
                pessoaLista: callback.content
            }
        case PESSOA_ACTIONS.EXCLUIR:
            return {
                ...state,
                pessoaLista: state.pessoaLista.filter( pessoa => {
                    return pessoa.id != callback.content
                })
            }
        case PESSOA_ACTIONS.BUSCAR:
            return {
                ...state,
                pessoaItem: callback.content
            }
        case PESSOA_ACTIONS.SALVAR:
            return {
                ...state,
                pessoaItem: callback.content,
                pessoaLista: state.pessoaLista.concat(callback.content)
            }
        case PESSOA_ACTIONS.ALTERAR:
            return {
                ...state,
                pessoaItem: callback.content,
                pessoaLista: state.pessoaLista.map( pessoa => {
                    if(pessoa.id == callback.content.id){
                        return callback.content;
                    } else {
                        return pessoa;
                    }
                })
            }
        default:
            return state;
    
    }



}
